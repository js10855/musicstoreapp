"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var State = /** @class */ (function () {
    function State(id, countryid, name) {
        this.id = id;
        this.countryid = countryid;
        this.name = name;
    }
    return State;
}());
exports.State = State;
//# sourceMappingURL=state.js.map