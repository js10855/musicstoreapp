"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Article = /** @class */ (function () {
    function Article(Id, Title, Summary) {
        this.Id = Id;
        this.Title = Title;
        this.Summary = Summary;
    }
    return Article;
}());
exports.Article = Article;
//# sourceMappingURL=article.js.map